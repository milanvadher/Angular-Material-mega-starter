import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs/Observable';
import { User } from '@firebase/auth-types';
import { Router } from '@angular/router';
import * as firebase from 'firebase';

@Injectable()
export class AuthService {

  isAuthenticate: boolean;
  userProfile: firebase.database.Reference;
  currentUser: firebase.User;

  constructor(public fAuth: AngularFireAuth, private router: Router) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}/userData/`);
      }
    });
  }

  async signup(email, password, fname, lname): Promise<User> {
    try {
      const newUser: User = await this.fAuth.auth.createUserWithEmailAndPassword(email, password);
      await newUser.sendEmailVerification().then(() => {
        localStorage.setItem('email', email);
        this.router.navigateByUrl('/emailverification');
      }).then(() => {
        firebase.database().ref('/userProfile/').child(newUser.uid).set({
          id: newUser.uid,
          email: email,
          firstname: fname,
          lastname: lname,
        });
      });
      return newUser;
    } catch (error) {
      console.error(error);
      throw new Error();
    }
  }

  login(email, password) {
    this.fAuth.auth.signInWithEmailAndPassword(email, password)
      .then(() => {
        this.currentUser = firebase.auth().currentUser;
        if (this.currentUser.emailVerified === true) {
          const token = localStorage.setItem('Login', 'done');
          this.isAuthenticate = true;
          this.router.navigateByUrl('/home');
        } else {
          console.log('Email id is not verified');
        }
      })
      .catch(err => {
        console.log('Error : ', err);
      });
  }

  logoutUser() {
    return this.fAuth.auth.signOut().then(() => {
      localStorage.clear();
      this.router.navigateByUrl('/login');
    });
  }

  resetPassword(email) {
    firebase.auth().sendPasswordResetEmail(email);
  }

}
