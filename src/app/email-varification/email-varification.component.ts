import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-email-varification',
  templateUrl: './email-varification.component.html',
  styleUrls: ['./email-varification.component.css']
})
export class EmailVarificationComponent implements OnInit {

  id: string;
  constructor() { }

  ngOnInit() {
    this.id = localStorage.getItem('email');
    console.log(this.id);
  }

}
