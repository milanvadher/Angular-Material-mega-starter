import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

export class User {
  email: string;
}

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  public user: User = new User();

  constructor(
    private _auth: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  resetPassword() {
    this._auth.resetPassword(this.user.email);
  }

}
