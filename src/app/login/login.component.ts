import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

export class User {
  email: string;
  password: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: User = new User();

  constructor(
    private _auth: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  login() {
    this._auth.login(this.user.email, this.user.password);
  }

}
