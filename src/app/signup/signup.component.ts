import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

export class User {
  fname: string;
  lname: string;
  email: string;
  password: string;
  repassword: string;
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public user: User = new User();

  constructor(
    private _auth: AuthService,
    public router: Router
  ) { }

  ngOnInit() {
  }

  signup() {
    if (this.user.password === this.user.repassword) {
      this._auth.signup(this.user.email, this.user.password, this.user.fname, this.user.lname);
    } else {
      console.log('password and retype password does not match');
    }
  }

}
