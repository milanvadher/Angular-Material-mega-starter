// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDHFo8KGyTWFh3rXHUoiDLmNblEnLZSghM',
    authDomain: 'angular-material-mega-starter.firebaseapp.com',
    databaseURL: 'https://angular-material-mega-starter.firebaseio.com',
    projectId: 'angular-material-mega-starter',
    storageBucket: '',
    messagingSenderId: '988607598381'
  }
};
